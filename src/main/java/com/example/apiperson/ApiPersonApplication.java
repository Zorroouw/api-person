package com.example.apiperson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ApiPersonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPersonApplication.class, args);
	}

}
