package com.example.apiperson.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.apiperson.dto.PersonDTO;
import com.example.apiperson.entities.Person;
import com.example.apiperson.interfaces.PersonService;
import com.example.apiperson.repositories.PersonRepository;



@Service
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	private PersonRepository repository;

	@Override
	public PersonDTO getPerson(Integer id) {
		Optional<Person> optionalPerson = this.repository.findById(id);
		if (optionalPerson.isPresent()) {
			Person entity = optionalPerson.get();
			PersonDTO response = this.getPersonDTO(entity);
			return response;
		}
		return null;
	}
	
	private PersonDTO getPersonDTO(Person entity) {

		PersonDTO response = new PersonDTO();

		response.setId(entity.getId());
		response.setName(entity.getName());
		response.setLastname(entity.getLastname());

		return response;
	}

	@Override
	public Boolean addPerson(PersonDTO dto) {
		Person entity = this.getPersonEntity(dto);
		if(entity.getName() == "" || entity.getLastname() == "") {
			return false;
		}
    	this.repository.insert(entity);
		return true;
	}
	
	private Person getPersonEntity(PersonDTO dto) {
		Person response = new Person();

		response.setId(dto.getId());
		response.setName(dto.getName());
		response.setLastname(dto.getLastname());
		return response;
	}

	@Override
	public Boolean updatePerson(Integer id, PersonDTO dto) {
		Optional<Person> optionalPerson = this.repository.findById(id);
		if(optionalPerson.isPresent()) {
			Person entity = getUpdatePersonEntity(dto, id);
			this.repository.save(entity);
			return true;
		}
		return false;
	}

	private Person getUpdatePersonEntity(PersonDTO dto, Integer id) {
		Person response = new Person();

		response.setId(id);
		response.setName(dto.getName());
		response.setLastname(dto.getLastname());
		return response;
	}

	@Override
	public Boolean deletePerson(Integer id) {
		Optional<Person> optionalPerson = this.repository.findById(id);
		if(optionalPerson.isPresent()) {
			Person entity = optionalPerson.get();
			this.repository.delete(entity);
			return true;
		}
		return false;
	}

	@Override
	public List<PersonDTO> getAllPerson() {
		List<Person> listPerson = this.repository.findAll();
		if(listPerson != null) {
			List<PersonDTO> response = new ArrayList<PersonDTO>();
			for(Person item : listPerson) {
				response.add(this.getAllPersonDTO(item));
			}
			return response;
		}
		return null;
	}
	
	private PersonDTO getAllPersonDTO(Person item) {
		PersonDTO response = new PersonDTO();
		response.setId(item.getId());
		response.setName(item.getName());
		response.setLastname(item.getLastname());
		return response;
	}

}
