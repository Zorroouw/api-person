package com.example.apiperson.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.apiperson.dto.PersonDTO;
import com.example.apiperson.interfaces.PersonService;



@RestController
@RequestMapping("/person")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PersonDTO> getPerson(@PathVariable("id") Integer id) {
		PersonDTO response = this.personService.getPerson(id);
		return new ResponseEntity<PersonDTO>(response, HttpStatus.OK);
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addPerson(@RequestBody PersonDTO dto)  {
		Boolean response = this.personService.addPerson(dto);
		return new ResponseEntity<Boolean>(response, HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> updatePerson(@PathVariable("id") Integer id, @RequestBody PersonDTO dto){
		Boolean response = this.personService.updatePerson(id, dto);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deletePerson(@PathVariable("id") Integer id){
		Boolean response = this.personService.deletePerson(id);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonDTO>> getAllPerson(){
		List<PersonDTO> response = this.personService.getAllPerson();
		return new ResponseEntity<List<PersonDTO>>(response, HttpStatus.OK);
	}
	
}
