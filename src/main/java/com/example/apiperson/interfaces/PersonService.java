package com.example.apiperson.interfaces;


import java.util.List;

import com.example.apiperson.dto.PersonDTO;

public interface PersonService {

	PersonDTO getPerson(Integer id);

	Boolean addPerson(PersonDTO dto);

	Boolean updatePerson(Integer id, PersonDTO dto);

	Boolean deletePerson(Integer id);

	List<PersonDTO> getAllPerson();

}
