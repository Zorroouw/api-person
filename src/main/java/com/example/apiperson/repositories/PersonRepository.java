package com.example.apiperson.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.apiperson.entities.Person;

@Repository
public interface PersonRepository extends MongoRepository<Person, Integer> {
	public Optional<Person> findById(Integer id);

}
